/**
* Theme: Ubold Admin
* Author: Coderthemes
* Page: opportunities
*/

!function($) {
    "use strict";

    var Opportunities = function() {};

    Opportunities.prototype.init = function () {
        
        //Pie Chart
        c3.generate({
             bindto: '#pie-chart',
            data: {
                columns: [
                    ['On Going', 46],
                    ['Completed', 24],       
                    ['New', 10]
          
                ],
                type : 'pie'
            },
            color: {
            	pattern: ["#34d3eb", "#7266ba", "#f05050",]
            },
            pie: {
		        label: {
		          show: false
		        }
		    }
        });
        
    },
    $.Opportunities = new Opportunities, $.Opportunities.Constructor = Opportunities

}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Opportunities.init()
}(window.jQuery);



